# encoding: utf-8

# This file is a part of WeRB project.
# License: LGPLv3+

require 'werb/errors'

module WeRB

  module Utils

    class << self

      def expand name, type = nil
        case type
        when Symbol
          prefix = '/' + type.to_s + '/'
          suffix = '.' + type.to_s
        when String
          case type[0]
          when '/'
            prefix = type + '/'
            suffix = ''
          when '.'
            prefix = '/'
            suffix = '.' + type
          else
            prefix = '/' + type.to_s + '/'
            suffix = '.' + type.to_s
          end
        else
          prefix = '/'
          suffix = ''
        end
        filename = prefix + name + suffix
        host = WeRB.options[:host]
        share = WeRB.options[:share]
        if File.exists?(host + filename)
          File.expand_path(host + filename)
        elsif File.exists?(share + filename)
          File.expand_path(share + filename)
        else
          nil
        end
      end

    end

  end

end
