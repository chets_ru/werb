# encoding: utf-8

# This file is a part of WeRB project.
# License: LGPLv3+

require 'werb/errors'
require 'werb/utils'
require 'werb/engine'

template :container, :template => <<-HTML
  <div id="%id%" class="%classes%" style="%style%" %attributes%>
   %content%
  </div>
HTML

template :panel, :template => <<-HTML
  <div id="%id%" class="%classes%" style="%style%" %attributes%>
   <div id="%id%-rightbox" class="right panel-sidebox %classes%" data-panel="%id%" data-side="right">&nbsp;</div>
   <div id="%id%-leftbox" class="left panel-sidebox %classes%" data-panel="%id%" data-side="left">&nbsp;</div>
   <div id="%id%-inner" class="panel-innerbox %classes%">
    %content% &nbsp;
   </div>
  </div>
HTML

template :upper, :template => <<-HTML
  <a href="#" id="werb-upper-anchor">
   <div id="%id%" class="%classes%" style="%style%" %attributes%>
    %content% &nbsp;
   </div>
  </a>
HTML

module WeRB

  class Container < Component

    template! :container

  end

  class Panel < Container

    template! :panel

    property :left, :class => Container, :default => nil
    property :right, :class => Container, :default => nil
    property :points, :class => Set, :default => Set[5, 50, 95]
    property :position, :class => Fixnum, :default => 1

    def attributes
      a = super
      a[:data_left] = left.id.to_name if left
      a[:data_right] = right.id.to_name if right
      a[:data_points] = points.to_json if points
      a[:data_position] = position.to_json if position
      a
    end

    def left= value
      if Symbol === value
        value = owner.find value
      end
      if value
        self.right = nil
      end
      if Panel === @left && @left.right == self
        @left.right = nil
      end
      if Component === value
        @left = value
        if Panel === value && value.right != self
          value.right = self
        end
      else
        @left = nil
      end
    end

    def right= value
      if Symbol === value
        value = owner.find value
      end
      if value
        self.left = nil
      end
      if Panel === @right && @right.left == self
        @right.left = nil
      end
      if Component === value
        @right = value
        if Panel === value && value.left != self
          value.left = self
        end
      else
        @right = nil
      end
    end

    def points= value
      if Set === value
        @points = value
      else
        @points = nil
      end
    end

    def position= value
      if Fixnum === value
        @position = value
      else
        @position = nil
      end
    end

    def get_html *args
      if @left
        style! ({ :left => "#{@points.to_a[@position]}%", :right => '0px' })
      elsif @right
        style! ({ :left => '0px', :right => "#{@points.to_a[@position]}%" })
      end
      super({ :attributes => attributes }, *args)
    end

  end

  class Upper < Component

    template! :upper

  end

  class Item < WeRB::Object

    def upper opts = {}, &block
      WeRB::Upper.new self, :werb_upper, opts, &block
    end

  end

end
