# encoding: utf-8

# This file is a part of WeRB project.
# License: LGPLv3+

require 'pp'
require 'set'
require 'rack'
require 'werb/core'
require 'werb/errors'
require 'werb/utils'
require 'werb/json'

class Array

  def get_data *args
    self.map { |v| v.get_data *args }
  end

  def get_html *args
    self.map { |v| v.get_html *args }
  end

end

class Hash

  def get_data *args
    result = self.dup
    result.each do |key, value|
      result[key] = value.get_data self, *args
    end
    result
  end

  def get_html *args
    result = self.dup
    result.each do |key, value|
      result[key] = value.get_html self, *args
    end
    result
  end

end

class Object

  def get_data *args
    self
  end

  def get_html *args
    self
  end

end

class Proc

  def get_data *args
    self.call *args
  end

  def get_html *args
    self.call *args
  end

  def get_call *args
    self.call *args
  end

end

class String

  def get_vars
    self.scan(/%(\w+)%/).flatten.map(&:intern).to_set
  end

  private :get_vars

  attr_reader :vars

  def vars
    @vars ||= get_vars
    @vars
  end

  def get_data *args
    result = self.dup
    self.vars.each do |var|
      result.gsub! "%#{var}%", var.get_data(*args).to_s
    end
    result
  end

  def get_html *args
    result = self.dup
    self.vars.each do |var|
      result.gsub! "%#{var}%", var.get_html(*args).to_s
    end
    result
  end

end

class Symbol

  def get_data *args
    args.each do |arg|
      if arg.respond_to? :has_key
        return arg[self].get_data(*args) if arg.has_key? self
      elsif arg.respond_to? :[]
        value = arg[self]
        return value.get_data(*args) if value != nil
      end
    end
    self.to_name
  end

  def get_html *args
    args.each do |arg|
      if arg.respond_to? :has_key
        return arg[self].get_html(*args) if arg.has_key? self
      elsif arg.respond_to? :[]
        value = arg[self]
        return value.get_html(*args) if value != nil
      end
    end
    self.to_name
  end

end

module WeRB

  class << self

    property :options, :class => Hash

  end

  module Engine

    DATA_EXPIRES = 60

    @@items = {}

    class << self

      def mkarg hash, method = :get_data
        result = {}
        hash.each do |key, value|
          if /^[\w\-]+$/ === key && value.respond_to?(method)
            result[key.to_key] = (Hash === value) ? mkarg(value) : value
          end
        end
        result
      end

      def mkparray path
        path = path.gsub /^http(s)?:\/[\w\-\.]+\//, ''
        path.gsub! /\?.*$/, ''
        path.split(/\/+/).delete_if { |i| i == '' } .map(&:intern)
      end

      def diffparray a, b
        level = a.size
        a.each_with_index do |item, idx|
          if item != b[idx]
            level = idx
            break
          end
        end
        {
          :level => level,
          :rest => a[level..-1]
        }
      end

      def data_headers expires
        {
          'Content-Type' => 'application/json; charset=UTF-8',
          'X-Accel-Expires' => expires.to_s,
          'Expires' => (Time.now + expires).httpdate
        }
      end

      private :mkarg, :mkparray, :diffparray

      def call env

        vars = mkarg env
        request = Rack::Request.new env
        query = mkarg request.GET
        if request.post? || request.put?
          if request.media_type == 'application/json'
            post = JSON.parse request.body.read
          else
            post = mkarg request.POST
          end
        else
          post = {}
        end
        cookies = mkarg request.cookies
        session = {} # TODO

        if /application\/json/ === vars[:http_accept]
          method = :get_data
        else
          method = :get_call
        end

        host = vars[:http_host]
        path = vars[:request_path]
        referer = vars[:http_referer]
        host_re = Regexp.compile('http(s)?' + Regexp.escape('://' + host + '/'))
        referer_local = (host_re === referer)
        path_array = mkparray path
        info = {
          :method => method,
          :path_array => path_array,
          :referer_local => referer_local
        }
        if referer_local
          referer_array = mkparray referer
          info[:referer_array] = referer_array
          info[:referer_diff] = diffparray path_array, referer_array
        else
          info[:referer_array] = info[:referer_diff] = nil
        end

        handler = get(path, method) || get(404, method)
        begin
          if method == :get_data
            data = handler.get_data info, session, cookies, post, query, vars
            expires = handler.respond_to?(:data_expires) &&
                handler.data_expires || DATA_EXPIRES
            result = {
              :status => 200,
              :headers => data_headers(expires),
              :body => JSON.stringify(data)
            }
          else
            result = handler.get_call info, session, cookies, post, query, vars
          end
        rescue WeRB::Error => e
          handler = get e.status
          if method == :get_data
            data = handler.get_data info, session, cookies, post, query, vars,
                :error => e
            expires = handler.respond_to?(:data_expires) &&
                handler.data_expires || DATA_EXPIRES
            result = {
              :status => e.status,
              :headers => data_headers(expires),
              :body => JSON.stringify(data)
            }
            result[:headers].merge! e.headers
          else
            result = handler.get_call info, session, cookies, post, query, vars,
                :error => e
          end
          result[:headers] = e.headers.merge result[:headers]
        rescue StandardError => e
          handler = get 500, method
          if method == :get_data
            data = handler.get_data info, session, cookies, post, query, vars,
                :error => e
            expires = handler.respond_to?(:data_expires) &&
                handler.data_expires || DATA_EXPIRES
            result = {
              :status => e.status,
              :headers => data_headers(expires),
              :body => JSON.stringify(data)
            }
          else
            result = handler.get_call info, session, cookies, post, query, vars,
                :error => e
          end
        end
        response = Rack::Response.new
        response.status = result[:status]
        response.headers.merge! result[:headers]
        response.write result[:body]
        response.finish
      end

      def register path, item
        if String === path
          if path[0] != '/'
            path = '/' + path
          end
          if path[-1] != '/'
            path = path + '/'
          end
          path = Regexp.compile "^#{Regexp.escape path}"
        elsif Symbol === path
          path = Regexp.compile "^/#{path}/"
        end
        @@items[path] ||= []
        @@items[path] << item
        path
      end

      def get path, method
        @@items.each do |key, value|
          if key === path
            value.each do |item|
              if item.respond_to? method
                return item
              end
            end
          end
        end
        nil
      end

    end

  end

  class Object

    property :options, :class => Hash

    def initialize opts = {}
      options! opts
      self.class.properties.each do |key, value|
        if key != :options
          old = self.instance_variable_get "@#{key}".intern
          opt = @options.delete key if ! old
          self.send "#{key}=".intern, opt if opt
        end
      end
    end

  end

  class Template < WeRB::Object

    @@items = {}

    class << self

      def [] id
        case id
        when Symbol
          @@items[id]
        when Template
          id
        else
          nil
        end
      end

      def []= id, template
        case template
        when Symbol
          @@items[id] = Template[template]
        when Template
          @@items[id] = template
        when String
          @@items[id] = Template.new template
        end
      end

    end

    property :id, :class => Symbol, :default => nil
    property :template, :file, :class => String, :default => nil
    property :options, :class => Hash

    def file= value
      @file = Utils.expand value, '/tmpl/'
    end

    def template
      if @file && ! @template
        @template = File.read @file
      end
      @template
    end

    def initialize id, opts = {}
      id! id
      super opts
      @@items[id] = self
    end

    def get_html *args
      self.template.get_html *args
    end

  end

  class Item < WeRB::Object

    property :template, :class => Template, :default => nil
    property :path, :class => Regexp, :default => nil

    def template= value
      @template = Template[value]
    end

    def path= value
      @path = Engine.register value, self
    end

    def initialize opts = {}, &block
      super opts
      @children = []
      if block_given?
        @body = self.instance_eval &block
      else
        @body = nil
      end
    end

    def add child
      @children << child
    end

    def delete child
      @children.delete child
    end

    def find child
      @children.each do |c|
        if c == child || c.respond_to?(:id) && c.id == child
          return c
        end
      end
      return nil
    end

    def [] name
      @options[name]
    end

    def get_data *args
      data = { :id => @id, :content => [], :body => @body }
      data.merge! @options
      @children.each do |item|
        data[:content] << item.get_data(self, *args)
      end
      data
    end

    def get_html *args
      data = { :id => @id, :content => '', :body => @body }
      data.merge! @options
      @children.each do |item|
        if item.id
          data[item.id] = item.get_html(self, *args)
        else
          data[:content] << item.get_html(self, *args)
        end
      end
      self.template.get_html data, self, *args
    end

    class Proc < ::Proc

      property :id, :class => Symbol, :default => nil
      property :owner, :class => Item, :default => nil
      property :options, :class => Hash

      def owner= value
        if @owner
          @owner.delete self
        end
        @owner = value
        if @owner
          @owner.add self
        end
      end

      def initialize owner, id, opts = {}, &block
        id! id
        owner! owner
        options! opts
        self.class.properties.each do |key, value|
          if key != :options
            old = self.instance_variable_get "@#{key}".intern
            opt = @options.delete key if ! old
            self.send "#{key}=".intern, opt if opt
          end
        end
        super &block
      end

      def get_data *args
        super @options, *args
      end

      def get_html *args
        super @options, *args
      end

    end

    def proc id, opts = {}, &block
      Item::Proc.new self, id, opts, &block
    end

    private :proc

  end

  class Page < Item

    property :expires, :class => Fixnum, :default => 60

    def initialize path, opts = {}, &block
      path! path
      super opts, &block
    end

    def get_call *args
      {
        :status => 200,
        :headers => {
                     'Content-Type' => 'text/html; charset=UTF-8',
                     'X-Accel-Expires' => expires.to_s,
                     'Expires' => (Time.now + expires).httpdate
                    },
        :body => get_html(*args)
      }
    end

  end

  class Styles < Hash

    def get_html *args
      h = super *args
      h.map { |k, v| "#{k.to_name} : #{v.get_html *args};" }.join ' '
    end

  end

  class Classes < Set

    def get_html *args
      self.map(&:to_name).join ' '
    end

  end

  class Attributes < Hash

    def get_html *args
      h = super *args
      h.map { |k, v| "#{k.to_name}=\"#{v.get_html *args}\"" }.join ' '
    end

  end

  class Component < Item

    class << self

      property :template, :class => Template, :default => nil

      def template
        @template || superclass.respond_to?(:template) && superclass.template ||
            nil
      end

      def template= value
        @template = Template[value]
      end

      def inherited cls
        creator = cls.name.split('::')[-1].downcase.intern
        Item.send :define_method, creator do |id, opts = {}, &block|
          cls.new self, id, opts, &block
        end
        Item.send :private, creator
      end

      def htmlname
        self.name.gsub('::', '-').to_key
      end

    end

    def template
      @template || self.class.template || ''
    end

    property :id, :class => Symbol, :default => nil
    property :owner, :class => Item, :default => nil

    def owner= value
      if @owner
        @owner.delete self
      end
      @owner = value
      if @owner
        @owner.add self
      end
    end

    option :classes, :class => Classes
    option :style, :class => Styles

    property :attributes, :class => Attributes

    def initialize owner, id, opts = {}, &block
      owner! owner
      id! id
      classes! self.class.htmlname
      style! ({})
      attributes! ({})
      super opts, &block
    end

  end

  class Static < WeRB::Object

    property :expires, :class => Fixnum, :default => (60 * 60 * 24 * 7)
    property :mime, :class => String, :default => nil
    property :path, :class => Regexp, :default => nil

    def path= value
      @path = Engine.register value, self
    end

    def initialize path, opts = {}
      path! path
      super opts
    end

    def get_call *args
      filename = Utils.expand(:request_path.get_data *args)
      if filename
        begin
          type = @mime || Rack::Mime.mime_type(File.extname(filename))
          last = File.mtime filename
          body = File.read filename
        rescue IOError => e
          raise Error::Forbidden
        end
      else
        raise Error::NotFound
      end
      {
        :status => 200,
        :headers => {
                     'Content-Type' => type,
                     'X-Accel-Expires' => expires.to_s,
                     'Expires' => (Time.now + expires).httpdate,
                     'Last-Modified' => last.httpdate
                    },
        :body => body
      }
    end

  end

end

def template id, opts = {}
  WeRB::Template.new id, opts
end

def page path, opts = {}, &block
  WeRB::Page.new path, opts, &block
end

def static path, opts = {}
  WeRB::Static.new path, opts
end

WeRB::Error::HTTP.each do |key, value|
  handler = proc do |*args|
    {
      :static => key,
      :headers => { 'Content-Type' => 'text/html' },
      :body => "<h1>#{key} #{value}</h1>" +
               '<p>Check your address line and/or try the ' +
               '<a href="/">server root</a>.</p>'
    }
  end
  WeRB::Engine.register key, handler
end




