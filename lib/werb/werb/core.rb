# encoding: utf-8

# This file is a part of WeRB project.
# License: LGPLv3+

require 'pp'
require 'set'

class Object

  class << self

    def property *args
      @properties ||= {}
      if Hash === args[-1]
        opts = args[-1]
        args = args[0..-2]
      else
        opts = {}
      end
      cls = opts[:class]
      args.each do |arg|
        arg = arg.intern
        get = arg
        set = "#{arg}=".intern
        put = "#{arg}!".intern
        var = "@#{arg}".intern

        define_method get do
          if ! instance_variable_defined?(var)
            instance_variable_set(var, singleton_class.default(arg))
          end
          instance_variable_get(var)
        end
        define_method set do |value|
          instance_variable_set(var, value)
        end

        if cls <= Array
          define_method put do |*values|
            if ! instance_variable_defined?(var)
              instance_variable_set(var, singleton_class.default(arg))
            end
            send set, instance_variable_get(var) + values.flatten
            send get
          end

        elsif cls <= Hash
          define_method put do |*values|
            if ! instance_variable_defined?(var)
              instance_variable_set(var, singleton_class.default(arg))
            end
            values.each do |value|
              send set, instance_variable_get(var).merge(value)
            end
            send get
          end

        elsif cls <= Set
          define_method put do |*values|
            if ! instance_variable_defined?(var)
              instance_variable_set(var, singleton_class.default(arg))
            end
            send set,
                instance_variable_get(var).merge(values.flatten.to_set.flatten)
            send get
          end

        elsif cls <= Text
          define_method put do |*values|
            if ! instance_variable_defined?(var)
              instance_variable_set(var, singleton_class.default(arg))
            end
            send set, instance_variable_get(var) + "\n" + values.join("\n")
            send get
          end

        else
          define_method put do |value = :$undefined|
            if ! instance_variable_defined?(var)
              instance_variable_set(var, singleton_class.default(arg))
            end
            if value != :$undefined
              send set, value
            end
            send get
          end
        end

        @properties.merge! ({ arg.intern => opts })
      end
    end

    def properties
      @properties ||= {}
      if self.superclass.respond_to? :properties
        parent = self.superclass.properties
      else
        parent = {}
      end
      parent.merge @properties
    end

    def default name, preset = nil
      opts = self.properties[name.intern] || self.options[name.intern] || {}
      if opts.has_key? :default
        opts[:default]
      elsif opts[:class]
        opts[:class].new
      else
        preset
      end
    end

    def option *args
      @options ||= {}
      if Hash === args[-1]
        opts = args[-1]
        args = args[0..-2]
      else
        opts = {}
      end
      cls = opts[:class]
      args.each do |arg|
        arg = arg.intern
        get = arg
        set = "#{arg}=".intern
        put = "#{arg}!".intern

        define_method get do
          @options ||= {}
          @options[arg] ||= singleton_class.default(arg)
          @options[arg]
        end
        define_method set do |value|
          @options ||= {}
          @options[arg] = value
        end

        if cls <= Array
          define_method put do |*values|
            @options ||= {}
            @options[arg] ||= singleton_class.default(arg)
            send set, @options[arg] + values.flatten
            send get
          end

        elsif cls <= Hash
          define_method put do |*values|
            @options ||= {}
            @options[arg] ||= singleton_class.default(arg)
            values.each do |value|
              send set, @options[arg].merge(value)
            end
            send get
          end

        elsif cls <= Set
          define_method put do |*values|
            @options ||= {}
            @options[arg] ||= singleton_class.default(arg)
            send set, @options[arg].merge(values.flatten.to_set.flatten)
            send get
          end

        elsif cls <= Text
          define_method put do |*values|
            @options ||= {}
            @options[arg] ||= singleton_class.default(arg)
            send set, @options[arg] + "\n" + values.join("\n")
            send get
          end

        else
          define_method put do |value = :$undefined|
            @options ||= {}
            @options[arg] ||= singleton_class.default(arg)
            if value != :$undefined
              send set, value
            end
            send get
          end
        end

        @options.merge! ({ arg.intern => opts })
      end
    end

    def options
      @options ||= {}
      if self.superclass.respond_to? :options
        parent = self.superclass.options
      else
        parent = {}
      end
      parent.merge @options
    end

    private :property, :option

  end

end

class String

  def to_key
    self.gsub('-', '_').downcase.intern
  end

  def to_keys
    self.split(/\s/).map { |s| s.to_key }.to_set
  end

  def to_name
    self
  end

end

class Symbol

  def to_key
    self
  end

  def to_name
    self.to_s.gsub('_', '-')
  end

end

class Integer

  def to_key
    self
  end

  def to_name
    self.to_s
  end

end

class Array

  def to_keys
    self.map { |v| v.to_key }.to_set
  end

end

class Set

  def to_keys
    self.map { |v| v.to_key }.to_set
  end

end

class Text < String
end

def sandbox &block
  lambda do
    $SAFE = 4
    yield
  end.call
end

