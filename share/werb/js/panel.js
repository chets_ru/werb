/*
 * encoding: utf-8
 *
 * This file is a part of WeRB project.
 * License: LGPLv3+
 *
 */

WeRB.Panel = {
  clickSideHandler : function (event) {
    event = event || window.event;
    var element = event.target || event.srcElement;
    var panel = document.getElementById(element.getAttribute('data-panel'));
    var side = element.getAttribute('data-side');
  }
};

WeRB.Upper = {
  clickHandler : function (event) {
    event = event || window.event;
    if (window.scrollTo) {
      window.scrollTo(0, 0);
      if (event.preventDefault) {
        event.preventDefault();
      } else {
        event.returnValue = false;
      }
      return false;
    } else {
      return true;
    }
  }
};

WeRB.addEvent(window, 'load', function () {
  var sideboxes = document.getElementsByClassName('panel-sidebox');
  for (var i = 0, len = sideboxes.length; i < len; i++) {
    var element = sideboxes[i];
    WeRB.addEvent(element, 'click', WeRB.Panel.clickSideHandler);
  }
  var upper = document.getElementById('werb-upper');
  if (upper) {
    WeRB.addEvent(upper, 'click', WeRB.Upper.clickHandler);
  }
});
